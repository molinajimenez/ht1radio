    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author USER
 */
public class RadioTest {
    Radio radio;
    public RadioTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of frecAdelante method, of class Radio.
     */
    @Test
    public void testFrecAdelante() {
        System.out.println("frecAdelante");
        Radio instance = new Radio();
        instance.prender();
        String result = instance.frecAdelante();
        assertEquals("540.0", result);
        // TODO review the generated test code and remove the default call to fail.
        
    }
    
    /**
     * Test of frecAtras method, of class Radio.
     */
    @Test
    public void testFrecAtras() {
        System.out.println("frecAtras");
        Radio instance = new Radio();
        instance.prender();
        String result = instance.frecAtras();
        assertEquals("1610.0", result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of prender method, of class Radio.
     */
    @Test
    public void testPrender() {
        System.out.println("prender");
        Radio instance = new Radio();
        assertEquals(true, instance.prender());
    }

    /**
     * Test of apagar method, of class Radio.
     */
    @Test
    public void testApagar() {
        System.out.println("apagar");
        Radio instance = new Radio();

        assertEquals(false, instance.apagar());

    }

    /**
     * Test of cambioFrecuencia method, of class Radio.
     */
    @Test
    public void testCambioFrecuencia() {
        System.out.println("cambioFrecuencia");
        Radio instance = new Radio();
        instance.prender();
        String result = instance.cambioFrecuencia();
        assertEquals("87.90", result);
    }


    /**
     * Test of mostrarEstacionBoton method, of class Radio.
     */
    @Test
    public void testMostrarEstacionBoton() {
        System.out.println("mostrarEstacionBoton");
        int pos = 0;
        Radio instance = new Radio();
        
        String[] botones = new String[12];
        botones[0] = "540.0";
        
        instance.setBotones(botones);
        
        String result = instance.mostrarEstacionBoton(pos);
        assertEquals("540.0", result);
    }

    /**
     * Test of mostrarEstacion method, of class Radio.
     */
    @Test
    public void testMostrarEstacion() {
        System.out.println("mostrarEstacion");
        Radio instance = new Radio();
        instance.prender();
        String result = instance.mostrarEstacion();
        assertEquals("530", result);
    }

}
