

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Francisco Molina, Rodrigo Zea 
 */
public class Radio implements RadioI {
    private String[] botones = new String[12];
    private boolean encendido = false;
    private boolean isAM = false;
    private boolean isFM = false;
    private String frecActual;
    private boolean conectadoAM = false;
    
    /**
     * Adelanta la frecuencia, dependiendo del tipo del que sea esta, AM o FM. AM = multiplos de 10 FM = multiplos de 0.2
     * @return frecActual: la nueva frecuencia sintonizada
     */
    @Override
    public String frecAdelante() {
        //Parece complicado a simple vista, pero debido a que las estaciones se trabajarán con String es necesario parsear el tipo de dato antes
        //*****************************************
        //Adicionalmente se agrego la funcionalidad que si esta en la primer frecuencia retroceda hacia la ultima ya sea AM o FM. Esto funciona en frecAtras y frecAdelante
        float fA = Float.parseFloat(frecActual);
        //Si es multiplo de 10, sería AM
        if (fA%10 == 0){
            //Se va de 10 en 10, según el paper
            if(fA==1610){
                frecActual="530";
            } else {
            fA += 10;
            frecActual = fA + "";
            }
        }else{ 
            //Se va de 0.2 en 0.2, si no es AM, en el paper
            if(fA>=107.8997){
                frecActual="87.90";
            } else{
                fA += 0.2;

                frecActual = String.format("%.2f", fA);
            }
            
            }
        
        //print por cosas de debugging
        System.out.println(frecActual);
        //retorna la nueva frecuencia
        return frecActual;
    }

    /**
     * Regresa una frecuencia, depende de si esta es AM o FM.
     * @return frecActual: la nueva frecuencia sintonizada
     */
    @Override
    public String frecAtras() {
        //Completamente lo mismo que frecAdelante
        //Solo que ocurre una resta en vez de suma.
        //*****************************************
        //Adicionalmente se agrego la funcionalidad que si esta en la primer frecuencia retroceda hacia la ultima ya sea AM o FM. Esto funciona en frecAtras y frecAdelante
         System.out.println(frecActual);
        float fA = Float.parseFloat(frecActual);
        if (fA%10 == 0){
            if(fA==530){
                frecActual="1610";
                fA = Float.parseFloat(frecActual);
            }
            fA -= 10; 
            
            frecActual = fA + "";
        }else{
            if(fA<=87.90){
                frecActual="107.90";
                fA = Float.parseFloat(frecActual);
            }
            fA -= 0.2;
            frecActual = String.format("%.2f", fA);
        }
        
        System.out.println(frecActual);
        return frecActual;
    }

    /**
     * Prende el radio. Cambia el estado de este, sintonizandole a la primera frecuencia.
     * @return encendido: estado del radio. encendido = false es lo mismo que apagado.
     */
    @Override
    public boolean prender() {
        encendido = true;
        frecActual = "530";
        
        return encendido;
    }

    /**
     * Apaga el radio.
     * @return encendido: estado del radio. encendido = false es lo mismo que apagado.
     */
    @Override
    public boolean apagar() {
       encendido = false;
       System.out.println("Radio apagada");
       
       return encendido;
    }

    /**
     * Cambia el modo de la frecuencia, de AM a FM o vice versa.
     * @return frecActual: la nueva frecuencia sintonizada, el cambio de AM o FM
     */
    @Override
    public String cambioFrecuencia() {
        //SUPONGO que este metodo es para switchear entre AM y FM?
        //Así que eso hace...
        float fA = Float.parseFloat(frecActual);
        //Si la frecuencia era un multiplo de 10, obviamente era una AM, entonces cambiaría a la FM "básica"
        //Pregunta: No sé si ponemos la frecuencia equivalente en vez de que mande al user a la primera frecuencia AM/FM
        //RESUELTO.
        if (fA%10 == 0){
            frecActual = "87.90"; 
        }else{ 
            frecActual = "530";
        }
        
        return frecActual;
    }

    /**
     * Guarda una frecuencia AM o FM en la radio
     * @param frec: la frecuencia AM o FM en entero para guardar
     * @param pos: la posición del arreglo donde se guardará dicha frecuencia
     */
    @Override
    public void guardarFrec(float frec, int pos) {
        String frecG = frec + "";
        botones[pos] = frecG;
    }
    
    //ALGO MÁS PARA AGREGAR A LA INTERFAZ

    /**
     * Devuelve una estación salvada en un botón
     * @param pos: La posición del botón del cuál se obtendrá una estación
     * @return estacion: la estación almacenada en el botón
     */
    @Override
    public String mostrarEstacionBoton(int pos) { 
        String estacion = botones[pos];
        return estacion;
    }
    
    @Override
    public String mostrarEstacion() {
        String estacion=this.frecActual;
        return estacion;
    }
    
    //GETTERS Y SETTERS
    public String[] getBotones() {
        return botones;
    }

    public void setBotones(String[] botones) {
        this.botones = botones;
    }

    public boolean isEncendido() {
        return encendido;
    }

    public void setEncendido(boolean encendido) {
        this.encendido = encendido;
    }

    public boolean isIsAM() {
        return isAM;
    }

    public void setIsAM(boolean isAM) {
        this.isAM = isAM;
    }

    public boolean isIsFM() {
        return isFM;
    }

    public void setIsFM(boolean isFM) {
        this.isFM = isFM;
    }

    public String getFrecActual() {
        return frecActual;
    }

    public void setFrecActual(String frecActual) {
        this.frecActual = frecActual;
    }

    public boolean isConectadoAM() {
        return conectadoAM;
    }

    public void setConectadoAM(boolean conectadoAM) {
        this.conectadoAM = conectadoAM;
    }
    
    
    
}
